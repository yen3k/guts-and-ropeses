port module Main exposing (main)

import Browser
import Element exposing (Element, image, centerX, rgb255)
import Element.Background as Background
import Element.Events exposing (onMouseEnter, onMouseLeave, onClick)
import Html exposing (Html)
import Random

main : Program () Model Msg
main =
  Browser.element
    { init = init
    , subscriptions = subscriptions
    , update = update
    , view = view
    }

type alias Model =
  { logoSrc: String
  , musicPlaying: Bool
  }

type Msg
  = EnterLogo
  | LeaveLogo
  | LogoClicked
  | PlayLightning (Int, Int)

port playMusic : () -> Cmd msg
port lightning : (Int, Int) -> Cmd msg

init : () -> ( Model, Cmd Msg )
init _ =
  ( Model "images/gar-logo.png" False
  , Cmd.none )

subscriptions : Model -> Sub Msg
subscriptions _ =
  Sub.batch []

randomAudioNumbers : Random.Generator (Int, Int)
randomAudioNumbers =
  Random.pair (Random.int 1 7) (Random.int 0 100)

playLightning : Cmd Msg
playLightning =
  Random.generate PlayLightning randomAudioNumbers

update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
  case msg of
    EnterLogo ->
      ( { model | logoSrc = "images/gar-logo-glow.png" }
      , Cmd.none
      )
    LeaveLogo ->
      ( { model | logoSrc = "images/gar-logo.png" }
      , Cmd.none
      )
    LogoClicked ->
      if model.musicPlaying == False then
        ( { model | musicPlaying = True }
        , playMusic () )
      else
        ( model, playLightning)
    PlayLightning (n, j) ->
      case j < 35 of
        True ->
          let
              jj = (modBy 3 j) + 1
          in
            ( model, lightning (n, jj))

        False ->
            ( model, lightning (n, -1))


logo : Model -> Element Msg
logo model =
  image
    [centerX, onMouseEnter EnterLogo, onMouseLeave LeaveLogo, onClick LogoClicked]
    { src = model.logoSrc, description = "vodka"}

view : Model -> Html Msg
view model =
  Element.layout
    [ Background.color (rgb255 0 0 0)]
    (logo model)

